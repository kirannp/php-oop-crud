<?php 
session_start();
    include_once 'include/class.user.php';
    $user = new User();

    $uid = $_SESSION['uid'];

    if (!$user->get_session()){
       header("location:login.php");
    }

    if ($_SESSION['utype'] == 0){
       header("location:account.php");

    }

    if (isset($_GET['q'])){
        $user->user_logout();
        header("location:login.php");
    }
?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Welcome <?php $user->get_fullname($uid); ?></title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  </head>
  <style>
  body{
   
    background: #e1eff0;
    background-repeat:no-repeat;
    background-size: 100%;
}

footer{

	margin-top: 20px;
	padding-top: 20px;
}
.bg__card__navbar{
	background-color: #000000;
}
.bg__card__footer{
	background-color: #000000 !important;
}
#add__new__list{
    top: -38px;
    right: 0px;
}
  </style>

<body>
<header>
  <div class="container bg-info p-5 ">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="dashboard.php">Dashboard</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link active" href="#">Users <span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link" href="account.php">Account</a>
          <a class="nav-item nav-link" href="donate.php">Donate</a>
          <a class="nav-item nav-link" href="registration.php">Registration</a>
          <a class="nav-item nav-link" href="index.php">Home</a>
          <a class="nav-item nav-link" href="dashboard.php?q=logout">Logout</a>

        </div>
      </div>
    </nav>
  </div>
</header>
<!---->
<main>
<div class="container my-5">
       <div class="card-body text-center">
    <h4 class="card-title">Hello <?php $user->get_fullname($uid); ?></h4>
    <p class="card-text">
    These are the users registered in the system...
    </p>
  </div>
  <?php 
  
          //fetching data in descending order (lastest entry first)
          $query = "SELECT * FROM users ORDER BY uid ASC";
          $result = $user->get_user_data($query);
          // echo '<pre>'; print_r($result); exit;

    ?>

    <div class="card">
        <button id="add__new__list" type="button" class="btn btn-success position-absolute" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fas fa-plus"></i> Add a new List</button>
        <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">User Name </th>
                <th scope="col">Fullname </th>
                <th scope="col">Update</th>
                <th scope="col">Email </th>
              </tr>
            </thead>
            <tbody>

            <?php 
        foreach ($result as $key => $res) {
        //while($res = mysqli_fetch_array($result)) {
                  
            echo "<tr>";
            echo "<th scope='row'>".$res['uid']."</th>";
            echo "<td>".$res['uname']."</td>";
            echo "<td>".$res['fullname']."</td>"; 
            echo "<td>
            <a class='btn btn-sm btn-primary' href=\"edit.php?id=$res[uid]\">Edit</a> |
             <a class='btn btn-sm btn-danger' href=\"delete.php?id=$res[uid]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";        
            echo "<td>".$res['uemail']."</td>";    
            echo "</tr>";
          }
        ?>
            </tbody>
          </table>
    </div>

    
    <!-- Large modal -->


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
          <div class="card-body text-center">
            <h4 class="card-title">Special title treatment</h4>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          </div>
            <div class=" card col-8 offset-2 my-2 p-3">
          <form>
            <div class="form-group">
              <label for="listname">List name</label>
              <input type="text" class="form-control" name="listname" id="listname" placeholder="Enter your listname">
            </div>
            <div class="form-group">
              <label for="datepicker">Deadline</label>
              <input  type="text" class="form-control" name="datepicker" id="datepicker" placeholder="Pick up a date">
            </div>
            <div class="form-group">
                                    <label for="datepicker">Add a list item</label>
                <div class="input-group">

                  <input type="text" class="form-control" placeholder="Add an item" aria-label="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button">Go!</button>
                  </span>
                </div>
              </div>
           <div class="form-group text-center">
             <button type="submit" class="btn btn-block btn-primary">Sign in</button>
          </div>
        </form>
    </div>
    </div>
  </div>
</div>
</div>
</main>
<!---->

<!---->
<footer >
  <div class="container bg-info p-5">
  
	</div>
</footer>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</html>