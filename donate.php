<?php 
session_start();
    include_once 'include/class.user.php';
    $user = new User();

    $uid = $_SESSION['uid'];

    if (!$user->get_session()){
       header("location:login.php");
    }

    if (isset($_GET['q'])){
        $user->user_logout();
        header("location:login.php");
    }

    if (isset($_POST['submit'])) { 
        $date = date("Y-m-d H:i:s");
        extract($_POST);

        $result = $user->execute("INSERT INTO donations (date,donation,user_id)
         VALUES('$date','$amount','$uid')");

        
    }

      $query = "SELECT * FROM donations WHERE user_id = '$uid' ORDER BY id ASC";
      $result = $user->get_user_data($query);


?>

<!DOCTYPE html>
<html>
<head>
    <title>Welcome <?php $user->get_fullname($uid); ?></title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/donation.css">
</head>
<body>
    <header>
      <div class="container bg-info p-5 ">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="dashboard.php">Dashboard</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-item nav-link" href="dashboard.php">Users</a>
              <a class="nav-item nav-link "  href="account.php">Account </a>
              <a class="nav-item nav-link active" href="#">Donate<span class="sr-only">(current)</span></a>
              <a class="nav-item nav-link" href="registration.php">Registration</a>
              <a class="nav-item nav-link" href="index.php">Home</a>
              <a class="nav-item nav-link" href="dashboard.php?q=logout">Logout</a>

            </div>
          </div>
        </nav>
      </div>
    </header>

    <div class="donations">
  <div class="donations__progress-bar"></div>
  <h2 class="donations__progress">
   Donate here</h2>
  <div class="donations__text">
    Join the 
    <span class="donations__donors">0</span>
    other donors who have already supported this project.
  </div>
  <form class="donations__form" method="post">

    <?php
 ?>
    <span class="donations__input-icon">$</span>
    <input class="donations__input" type="number" value="50" title="$" id="donation" name="amount" />
    <input type="submit" class="donations__submit" value="Donate" name="submit" />
    <!-- <button class="donations__reset" style="color: gray;border: 1px solid green;">Clear</button> -->
  </form>
</div>




    <footer >
  <div class="container bg-info p-5">
    <h1>Past donations</h1>
    <table class="table table-condensed">
    <thead>
      <tr>
        <th>Date</th>
        <th>Amount</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        foreach ($result as $key => $res) {
        //while($res = mysqli_fetch_array($result)) { 
            echo "<tr>";
            echo "<th scope='row'>".$res['id']."</th>";
            echo "<td>".$res['donation']."</td>";
            echo "<td>".$res['date']."</td>"; 
            echo "<td>              
              <a class='btn btn-sm btn-danger' href=\"delete.php?id=$res[id]\"
               onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";  
            echo "</tr>";
          }
        ?>

      
    </tbody>
  </table>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>