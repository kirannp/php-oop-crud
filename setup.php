<?php 
$conn = mysqli_connect("localhost","root","");
$create = "CREATE database 00167658_kiranojha_dw";
mysqli_query($conn,$create);

$con= mysqli_select_db($conn,"00167658_kiranojha_dw");

$query1 = "CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(30) DEFAULT NULL,
  `upass` varchar(255) DEFAULT NULL,
  `fname` varchar(32) NOT NULL,
  `lname` varchar(32) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `uemail` varchar(70) DEFAULT NULL,
  `visitor_reg_number` varchar(8) NOT NULL,
  `utype` int(11) NOT NULL
)";
mysqli_query($conn,$query1);

$query2 = "INSERT INTO `users` (`uid`, `uname`, `upass`, `fname`, `lname`, `fullname`, `uemail`, `visitor_reg_number`, `utype`) VALUES
(4, 'kiranji', '". '$2y$10$fCEE5rBXvYyOc7rGIqNUp.l4JvwxrRnfxUdsIlaGD1WO5YoLbKX4K' .", 'Kiran', 'Ojha', 'Kiran Ojha', 'kiran@kiran.com', 'PP225145', 1),
(5, 'test', '". '$2y$10$oWLUbDl9Wq1aG7bacUGSCOMiH/7llC2xQXhuo9Su2SgVjmc8910sW' .", 'Kiran', 'Ojha', 'Kiran ojha', 'test@test.com', 'PP225546', 0),
(6, 'ashish', '". '$2y$10$2xbMclDxXGUQwQZ6usxPE.QiNOBfWIMX7aJC7.LLjYooETl2Bwu42' .", '', '', 'Aashish ghimire', 'a@a.com', 'PP464515', 0),
(7, 'kamal1', '". '$2y$10$JSlBpXKJyZhTigdVmslLJeqobYtC83sLF.R7MVDd.zjuJ7p4uvyR.' .", 'Kamal', 'Ojha', 'Kamal ojha', 'kamal@mail.com', 'PP913673', 0)
";
mysqli_query($conn,$query2);

$query3 = "ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);";
mysqli_query($conn,$query3);

$query4 = "ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`)";
mysqli_query($conn,$query4);

$query5 = "CREATE TABLE `donations` (
  `id` int(11) NOT NULL,
  `donation` int(20) NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL
)";
mysqli_query($conn,$query5);

$query6 = "INSERT INTO `donations` (`id`, `donation`, `date`, `user_id`) VALUES
(3, 60, '2019-07-29 06:38:54', 4),
(4, 50, '2019-07-29 06:39:28', 4),
(5, 50, '2019-07-29 06:42:20', 4),
(6, 50, '2019-07-29 06:42:48', 4),
(7, 50, '2019-07-29 06:43:10', 4),
(8, 50, '2019-07-29 06:43:28', 4),
(9, 50, '2019-07-29 06:43:32', 4),
(10, 50, '2019-07-29 06:44:00', 4),
(11, 50, '2019-07-29 06:44:08', 4),
(12, 50, '2019-07-29 06:49:28', 5),
(13, 10000, '2019-07-29 06:49:32', 5)";
mysqli_query($conn,$query6);

$query7 = "ALTER TABLE `donations`
  ADD PRIMARY KEY (`id`)";
mysqli_query($conn,$query7);

$query8 = "ALTER TABLE `donations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14";
mysqli_query($conn,$query8);

$query9 = "ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT";
mysqli_query($conn,$query9);




?>