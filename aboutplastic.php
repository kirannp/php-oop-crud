<?php 
include_once 'include/class.user.php';
$user = new User();

if (isset($_POST['submit'])){
        extract($_POST);
        $register = $user->reg_user($name, $firstname,$lastname, $username, $password, $email);
        if ($register) {
            // Registration Success
            echo "<div style='text-align:center'>Registration successful <a href='login.php'>Click here</a> to login</div>";
        } else {
            // Registration Failed
            echo "<div style='text-align:center'>Registration failed. Email or Username already exits please try again.</div>";
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>PlasticPollutions</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/footer.css">

	<meta name="description" content="PlasticPollutions are an environmentally friendly action group whose aim is to stop plastic waste harming oceans and wildlife.   They are promoting action to stop the flow of plastic pollution into our environment and they are looking to manufacturers and retailers to help phase out non-essential, single-use plastics."/>

</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">PlasticPollutions</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">Home
              </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="aboutplastic.php">About
                <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Campaigns</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Contact</a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="#">Strategy</a>
        </li> <li class="nav-item">
          <a class="nav-link" href="#">Latest on Plastic</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="login.php">Login</a>
        </li>
        <li class="nav-item">
          <a type="button" class="btn btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Sign up Now</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Page Content -->
<section class="py-5">
  <div class="container">
    <h1 class="display-4">Different types of plastic</h1>
     <p class="lead">
       Plastic is an essential component of many items, including water bottles, combs, and beverage containers. Knowing the difference, as well as the SPI codes, will help you make more informed decisions about recycling.
        The seven types of plastic include:
        <ol>
        <li>Polyethylene Terephthalate (PETE or PET)</li>
        <li>High-Density Polyethylene (HDPE)</li>
        <li>Polyvinyl Chloride (PVC)</li>
        <li>Low-Density Polyethylene (LDPE)</li>
        <li>Polypropylene (PP)</li>
        <li>Polystyrene or Styrofoam (PS)</li>
       <li> Miscellaneous plastics (includes: polycarbonate, polylactide, acrylic, acrylonitrile butadiene, styrene, fiberglass, and nylon)</li>
      </ol>
     </p>
    <h1 class="display-4">What to do about plastic? </h1>
    <p class="lead">In 2009, the United Nations officially established International Mother Earth Day. Each year, the world celebrates it on April 22nd. In 2018, the central theme for the day will be “End Plastic Pollution.” With tons of plastic that end up in our oceans, rivers, and other waterways, this is an essential topic which requires much attention from the global community. Today, there are hundreds of organizations committed to this theme. Let’s take a look at some of the biggest players working to rid the planet of plastic. </p>
    <hr>
    <h1 class="display-4">Why Ending Plastic Pollution is Important</h1>
    <p class="lead">Have you heard of the giant islands of plastic forming in our oceans? Did you know that plastic kills thousands of marine animals every year? Are you aware of the fact that plastic can contaminate the food we eat and the water we drink? All these are facts and are just the tip of the iceberg when it comes to plastic pollution. As a society, we have come to rely heavily on plastic for everything that we do and use. From our car and home to our food and personal hygiene, we use plastic all the time, and this is causing the planet to suffer. This year’s central theme of International Mother Earth Day, End Plastic Pollution, aims to raise the awareness of this issue and promote steadfast action to reduce and eventually eliminate plastic pollution. </p>
    </div>

    <div class="col-md-12 text-center">
<a href="donate.php" class="btn btn-info btn-lg">Donate now</a>
</div>
</section>


<!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    <b>Do you like cookies?</b> &#x1F36A; We use cookies to ensure you get the best experience on our website. <a href="#" target="_blank">Learn more</a>

    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
        I agree
    </button>
</div>
<!-- END Bootstrap-Cookie-Alert -->

<!-- Footer start -->

<div class="footer-basic">
            <section id="lab_social_icon_footer">
        <div class="container">
                <div class="text-center center-block">
                        <a href="#"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                      <a href="#"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                      <a href="#"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                      <a href="mailto:#"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
            </div>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Home</a></li>
                <li class="list-inline-item"><a href="#">Services</a></li>
                <li class="list-inline-item"><a href="#">About</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
                <li class="list-inline-item"><a href="#">Cookies Policy</a></li>
            </ul>
            <p class="copyright">PlasticPollutions © 2019</p>
        </div>
          </section>
    </div>

  <!-- Footer end -->

<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/cookiealert.js"></script>

</html>