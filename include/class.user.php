<?php
include "db_config.php";
class User {
    protected $db;
    public function __construct() {
        $this->db = new DB_con();
        $this->db = $this->db->ret_obj();
    }
    /*** for registration process ***/
    public function reg_user($name,$firstname,$lastname, $username, $password, $email) {
        $password = password_hash($password, PASSWORD_DEFAULT);
        //checking if the username or email is available in db
        $query = "SELECT * FROM users WHERE uname='$username' OR uemail='$email'";
        $result = $this->db->query($query) or die($this->db->error);
        $count_row = $result->num_rows;

        // to generate a unique user registration number ($pp_reg_number) following is done

        $rand = random_int(100000,999999); // output: random 6 digit number

        $pp_reg_number = str_pad($rand, 8, "PP", STR_PAD_LEFT); // output: adds PP in first and makes
                                                                // total digit = 8 eg: PP111111 

        // get all the registration numbers from db
        $query = "SELECT visitor_reg_number  FROM users";
        $reg_result = $this->db->query($query) or die($this->db->error);
        $reg_data = $reg_result->fetch_array(MYSQLI_ASSOC);

        // get value 
        $visitor_reg_number_db = $reg_data['visitor_reg_number'];

        // if reg number in db not equals to generated reg numbers, register
        if ($pp_reg_number != $visitor_reg_number_db ) {
            # code...

        //if the username is not in db then insert to the table
        // if utype = 0 the its a user account. Since there can be only one admin account, other accounts are made user account
        $utype = 0;
        if ($count_row == 0) {
            $query = "INSERT INTO users SET uname='$username', upass='$password',fname = '$firstname',lname = '$lastname', fullname='$name', uemail='$email',visitor_reg_number = '$pp_reg_number', utype = '$utype'";
            $result = $this->db->query($query) or die($this->db->error);
            return true;
        }

        }
         else {
            return false;
        }
    }
    /*** for login process ***/
    public function check_login($emailusername, $password) {
        //query for retrieving necessary datas for login
        $query = " SELECT * FROM users WHERE uname='$emailusername' OR uemail='$emailusername' ";
        $result = $this->db->query($query) or die($this->db->error);
        // fetching data from db
        $user_data = $result->fetch_array(MYSQLI_ASSOC);
        // getting hashvalue from database
        $uhash = $user_data['upass'];        
        // count the row to find if there are any data available
        $count_row = $result->num_rows;
        if ($count_row > 0) {
            // password_verify is builtin php function used to verify hashes
            if (password_verify($password, $uhash)) {

                $_SESSION['login'] = true; // this login var will use for the session thing
                $_SESSION['uid'] = $user_data['uid'];
                $_SESSION['utype'] = $user_data['utype'];
                return true;
            } else {
                return false;
            }
        }
	}

    public function editUser(){

    }
	

    // for executing a query
    // we will be using this function for create / update / delete of data
    public function execute($query) {
        $result = $this->db->query($query) or die($this->db->error);
        if ($result == false) {
            echo 'Error: cannot execute the command';
            return false;
        } else {
            return true;
        }
    }
    // This function is used to escape the data from the form. It prevents
    // sql injection attack to happen
    public function escape_string($value) {
        return $this->db->real_escape_string($value);
    }
    public function get_fullname($uid) {
        $query = "SELECT fullname FROM users WHERE uid = $uid";
        $result = $this->db->query($query) or die($this->db->error);
        $user_data = $result->fetch_array(MYSQLI_ASSOC);
        echo $user_data['fullname'];
    }
    public function get_user_data($query) {
        $result = $this->db->query($query);
        
        if ($result == false) {
            return false;
        } 
        
        $rows = array();
        
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        
        return $rows;
    }
    /*** starting the session ***/
    public function get_session() {
        return $_SESSION['login'];
    }
    public function user_logout() {
        $_SESSION['login'] = FALSE;
        unset($_SESSION);
        session_destroy();
	}

    

}
