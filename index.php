<?php 
include_once 'include/class.user.php';
$user = new User();

if (isset($_POST['submit'])){
        extract($_POST);
        $register = $user->reg_user($name, $firstname,$lastname, $username, $password, $email);
        if ($register) {
            // Registration Success
            echo "<div style='text-align:center'>Registration successful <a href='login.php'>Click here</a> to login</div>";
        } else {
            // Registration Failed
            echo "<div style='text-align:center'>Registration failed. Email or Username already exits please try again.</div>";
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>PlasticPollutions</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/footer.css">

	<meta name="description" content="PlasticPollutions are an environmentally friendly action group whose aim is to stop plastic waste harming oceans and wildlife.   They are promoting action to stop the flow of plastic pollution into our environment and they are looking to manufacturers and retailers to help phase out non-essential, single-use plastics."/>

</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">PlasticPollutions</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="aboutplastic.php">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Campaigns</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Contact</a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="#">Strategy</a>
        </li> <li class="nav-item">
          <a class="nav-link" href="latest.php">Latest on Plastic</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">How you can help</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="login.php">Login</a>
        </li>
        <li class="nav-item">
          <a type="button" class="btn btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Sign up Now</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" title="Use of plastic in agriculture" style="background-image: url('assets/img/1.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4">Plastic Pollutions</h2>
          <p class="lead">is environment friendly</p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" title="Plastic pollution in forest" style="background-image: url('assets/img/2.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4">Plastic Pollutions</h2>
          <p class="lead">plants trees on polluted area.</p>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" title="Plastic pollution" style="background-image: url('assets/img/3.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4">Plastic Pollutions</h2>
          <p class="lead">manages wastes.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
</header>

<!-- Page Content -->
<section class="py-5">
  <div class="container">
    <h1 class="display-4">Introduction

    <!-- Audio (assets/aboutus.mp3) plays on button clicked  -->
     <audio id="demo" src="assets/mp3/aboutus.mp3" type="audio/wav"></audio>
    <div>
    <button type="button" class="btn btn-outline-primary" onclick="document.getElementById('demo').play()">Read</button>
    </div>

     </h1>
    <p class="lead">PlasticPollutions are an environmentally friendly action group whose aim is to stop plastic waste harming oceans and wildlife.   They are promoting action to stop the flow of plastic pollution into our environment and they are looking to manufacturers and retailers to help phase out non-essential, single-use plastics.   
     
    The company was formed because the owners were really upset to hear about the tonnes of plastic that ends up in the sea each year and that approximately only 9% is recycled so they have set up a company to promote recycling.!</p>
    </div>
</section>

<div class="col-md-12 text-center">
<a href="donate.php" class="btn btn-info btn-lg">Donate now</a>
</div>

<!-- Twitter feed -->



<!-- signup popup form -->


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
          <div class="card-body text-center">
            <h4 class="card-title">Sign up Now</h4>
            <p class="card-text">Register an account and join us in this mission</p>
          </div>
            <div class=" card col-8 offset-2 my-2 p-3">
          <form method="post" name="reg">
          	<div class="form-group">
	              <label for="firstname">Firstname: </label>
	              <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Enter your Firstname">
            </div>

          	<div class="form-group">
	              <label for="lastname">Lastname: </label>
	              <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter your Lastname">
            </div>

            <div class="form-group">
	              <label for="name">Fullname: </label>
	              <input type="text" class="form-control" name="name" id="name" placeholder="Enter your Fullname">
            </div>

            <div class="form-group">
	              <label for="username">Username: </label>
	              <input type="text" class="form-control" name="username" id="username" placeholder="Enter your Username">
            </div>

            <div class="form-group">
	              <label for="email">Email: </label>
	              <input type="text" class="form-control" name="email" id="email" placeholder="Enter your Email">
            </div>

            <div class="form-group">
              	  <label for="password">Password: </label>
                  <input type="password" class="form-control" name="password" placeholder="Enter Password" aria-label="Search for...">
              </div>
           <div class="form-group text-center">
             <input class="btn-outline-primary" type="submit" name="submit" value="Sign up Now" onclick="return(submitreg());">
          </div>
        </form>
    </div>
    </div>
  </div>
</div>

<!-- End of signup popup form -->

<!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    <b>Do you like cookies?</b> &#x1F36A; We use cookies to ensure you get the best experience on our website. <a href="#" target="_blank">Learn more</a>

    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
        I agree
    </button>
</div>
<!-- END Bootstrap-Cookie-Alert -->

<!-- Footer start -->

<div class="footer-basic">
            <section id="lab_social_icon_footer">
        <div class="container">
                <div class="text-center center-block">
                        <a href="#"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                      <a href="#"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                      <a href="#"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                      <a href="mailto:#"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
            </div>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Home</a></li>
                <li class="list-inline-item"><a href="#">Services</a></li>
                <li class="list-inline-item"><a href="#">About</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
                <li class="list-inline-item"><a href="#">Cookies Policy</a></li>
            </ul>
            <p class="copyright">PlasticPollutions © 2019</p>
        </div>
          </section>
    </div>

  <!-- Footer end -->

<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/cookiealert.js"></script>
<script>
      function submitreg() {
        var form = document.reg;
        if (form.name.value == "") {
          alert("Enter fullname.");
          return false;
        } else if (form.username.value == "") {
          alert("Enter username.");
          return false;
        } else if (form.password.value == "") {
          alert("Enter password.");
          return false;
        } else if (form.email.value == "") {
          alert("Enter email.");
          return false;
        }else if (form.firstname.value == "") {
          alert("Enter username.");
          return false;
        }else if (form.lastname.value == "") {
          alert("Enter username.");
          return false;
        }
      }
    </script>
</body>
</html>