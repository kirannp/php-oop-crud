<?php 
session_start();
include_once 'include/class.user.php';
$user = new User();

if(isset($_SESSION['uid'])){
  header("location:account.php");
 }

if (isset($_POST['submit'])) { 
		extract($_POST);   
	    $login = $user->check_login($emailusername, $password);

      if(isset($_SESSION['num_login_fail'])){
          if($_SESSION['num_login_fail'] == 3)
           {
             if(time() - $_SESSION['last_login_time'] < 3*60*60 ) 
              {
                 // alert to user wait for 3 minutes afer
                  echo '<script language="javascript">';
                  echo 'alert("You have been blocked. Please try again after 3 minutes.")';
                  echo '</script>';
                  $_SESSION['blocked']= 'yes';
                  // return;
              }
              else
              {
                //after 3 minutes
                 $_SESSION['num_login_fail'] = 0;
              }
           }      
        }
         else
              {
                //after 3 minutes
                 $_SESSION['num_login_fail'] = 1;
              }

	    if ($login) {
	        // Registration Success
         $_SESSION['num_login_fail'] = 0;
	       header("location:dashboard.php");
	    } else {
	        // Registration Failed
          $_SESSION['num_login_fail'] ++;
          $_SESSION['last_login_time'] = time();
	        echo 'Wrong username or password';
	    }
	}
?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  </head>

  <body>
    <div id="container" class="container">
      <h1>Login Here</h1>
      <?php
       if(isset($_SESSION['blocked'])){ ?>
       <div  class="alert alert-danger"> You have been blocked for 3 unsuccessful login attempt. try again after 3 minutes 
        </div>
      <?php } ?>
      <form action="" method="post" name="login">
        <table class="table " width="400">
          <tr>
            <th>UserName or Email:</th>
            <td>
              <input type="text" name="emailusername" required>
            </td>
          </tr>
          <tr>
            <th>Password:</th>
            <td>
              <input type="password" name="password" required>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <?php
              if(isset($_SESSION['blocked'])){ ?>

              <input class="btn" type="submit" name="submit" value="Login" onclick="return(submitlogin());" disabled>

              <?php } else { ?>

              <input class="btn" type="submit" name="submit" value="Login" onclick="return(submitlogin()); ">

              <?php } ?>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a href="registration.php">Register new user</a></td>
          </tr>

        </table>
      </form>
    </div>
    <script>
      function submitlogin() {
        var form = document.login;
        if (form.emailusername.value == "") {
          alert("Enter email or username.");
          return false;
        } else if (form.password.value == "") {
          alert("Enter password.");
          return false;
        }
      }
    </script>


  </body>