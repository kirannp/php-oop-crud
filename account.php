<?php 
session_start();
    include_once 'include/class.user.php';
    $user = new User();

    $uid = $_SESSION['uid'];

    if (!$user->get_session()){
       header("location:login.php");
    }

    if (isset($_GET['q'])){
        $user->user_logout();
        header("location:login.php");
    }

    if(isset($_POST['update']))
	{ 
    
    $fname = $user->escape_string($_POST['firstname']);
    $lname = $user->escape_string($_POST['lastname']);
    $uname = $user->escape_string($_POST['username']);
    $fullname = $user->escape_string($_POST['name']);
    $uemail = $user->escape_string($_POST['email']);
    $visit_reg_num = $user->escape_string($_POST['visit_reg_num']);

    $result = $user->execute("UPDATE users SET fname='$fname',lname = '$lname',uname ='$uname',fullname= '$fullname',uemail='$uemail' WHERE uid=$uid AND visitor_reg_number = '$visit_reg_num'");
    if(!$result){

    }else{
	$_SESSION['msg'] = 'Successfully updated';
    header("Location: account.php");

    }


  	}  
    


	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Welcome <?php $user->get_fullname($uid); ?></title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
</head>
<body>
	<header>
	  <div class="container bg-info p-5 ">
	    <nav class="navbar navbar-expand-lg navbar-light bg-light">
	      <a class="navbar-brand" href="dashboard.php">Dashboard</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
	        <span class="navbar-toggler-icon"></span>
	      </button>
	      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
	        <div class="navbar-nav">
	          <a class="nav-item nav-link" href="dashboard.php">Users</a>
	          <a class="nav-item nav-link active"  href="account.php">Account <span class="sr-only">(current)</span></a>
	          <a class="nav-item nav-link" href="donate.php">Donate</a>
	          <a class="nav-item nav-link" href="registration.php">Registration</a>
	          <a class="nav-item nav-link" href="index.php">Home</a>
	          <a class="nav-item nav-link" href="dashboard.php?q=logout">Logout</a>

	        </div>
	      </div>
	    </nav>
	  </div>
	</header>

<?php 

		 //fetching data in descending order (lastest entry first)
          $query = "SELECT * FROM users WHERE uid = '$uid'";
          $result = $user->get_user_data($query);
          // echo '<pre>'; print_r($result); exit;
          foreach ($result as $res) {
		    $fname = $res['fname'];
		    $lname = $res['lname'];
		    $uname = $res['uname'];
		    $uemail = $res['uemail'];
		    $fullname = $res['fullname'];
		    $visit_reg_num = $res['visitor_reg_number'];

		}
	

        ?>

<main>



	<div class="card-body text-center">
            <h4 class="card-title">Update Account</h4>
            <p class="card-text">Update your information here</p>
            <?php
       if(isset($_SESSION['msg'])){ ?>
       <div  class="alert alert-primary" > <?php echo  $_SESSION['msg']; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button></div>
      <?php } ?>
          </div>
            <div class=" card col-8 offset-2 my-2 p-3">
          <form method="post" name="reg">
          	<div class="form-group">
	              <label for="firstname">Firstname: </label>
	              <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Enter your Firstname" value="<?php echo $fname;?>">
            </div>

          	<div class="form-group">
	              <label for="lastname">Lastname: </label>
	              <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter your Lastname" value="<?php echo $lname;?>">
            </div>

            <div class="form-group">
	              <label for="name">Fullname: </label>
	              <input type="text" class="form-control" name="name" id="name" placeholder="Enter your Fullname" value="<?php echo $fullname;?>">
            </div>

            <div class="form-group">
	              <label for="username">Username: </label>
	              <input type="text" class="form-control" name="username" id="username" placeholder="Enter your Email or Username" value="<?php echo $uname;?>">
            </div>

            <div class="form-group">
	              <label for="email">Email: </label>
	              <input type="text" class="form-control" name="email" id="email" placeholder="Enter your Email or Username" value="<?php echo $uemail;?>">
            </div>

            <input type="hidden" name="visit_reg_num" value="<?php echo $visit_reg_num;?>">

           <div class="form-group text-center">
             <input class="btn" type="submit" name="update" value="Update" onclick="return(submitreg());">
          </div>
        </form>
    </div>
</main>



<footer >
  <div class="container bg-info p-5">
  
	</div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
      function submitreg() {
        var form = document.reg;
        if (form.name.value == "") {
          alert("Enter fullname.");
          return false;
        } else if (form.username.value == "") {
          alert("Enter username.");
          return false;
        } else if (form.password.value == "") {
          alert("Enter password.");
          return false;
        } else if (form.email.value == "") {
          alert("Enter email.");
          return false;
        }else if (form.firstname.value == "") {
          alert("Enter firstname.");
          return false;
        }else if (form.lastname.value == "") {
          alert("Enter lastname.");
          return false;
        }
      }
    </script>
</body>
</html>