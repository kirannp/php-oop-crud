<?php 
session_start();
    include_once 'include/class.user.php';
    $user = new User();

    $uid = $_SESSION['uid'];

    if (!$user->get_session()){
       header("location:login.php");
    }

    if (isset($_GET['q'])){
        $user->user_logout();
        header("location:login.php");
    }
?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Welcome <?php $user->get_fullname($uid); ?></title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <style>
table, th, td {
    border: 1px solid black;
    padding: 10px 10px 10px 10px;
}

</style>
  </head>

  <body>
    <div id="container" class="container">
      <div id="header">
        <a href="home.php?q=logout">LOGOUT</a>
      </div>
      <div id="main-body">
        <br/>
        <br/>
        <br/>
        <br/>
        <h1>
              Hello <?php $user->get_fullname($uid); ?>
        </h1>
        <br>
        <?php 
  
          //fetching data in descending order (lastest entry first)
          $query = "SELECT * FROM users ORDER BY uid DESC";
          $result = $user->get_user_data($query);
          // echo '<pre>'; print_r($result); exit;

          ?>

          
        <table width='100%' border=0>
        
        <tr bgcolor='#CCCCCC'>
            <td>Name</td>
            <td>Username</td>
            <td>Hash</td>
            <td>Fullname</td>
            <td>Email</td>
            <td>Update</td>
        </tr>
        <?php 
        foreach ($result as $key => $res) {
        //while($res = mysqli_fetch_array($result)) {
                  
            echo "<tr>";
            echo "<td>".$res['uid']."</td>";
            echo "<td>".$res['uname']."</td>";
            echo "<td>".$res['upass']."</td>";
            echo "<td>".$res['fullname']."</td>"; 
            echo "<td>".$res['uemail']."</td>";    
            echo "<td><a href=\"edit.php?id=$res[uid]\">Edit</a> | <a href=\"delete.php?id=$res[uid]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";        
        }
        ?>
        </table>

      </div>

    
      <div id="footer"></div>
    </div>
  </body>

  </html>