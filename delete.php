<?php
//including the database connection file
include_once("include/class.user.php");
 
$user = new User();
 
//getting id of the data from url
$id = $user->escape_string($_GET['id']);
 
//deleting the row from table
$result = $user->execute("DELETE FROM donations WHERE id=$id");
// can be used too
 
if ($result) {
    //redirecting to the display page (index.php in our case)
    header("Location:donate.php");
}
?>

